package com.example.compose.controller;

import com.example.compose.entity.Counter;
import com.example.compose.service.CounterService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Slf4j
public class HelloController {
    private final CounterService counterService;

    @GetMapping("/hello")
    public String getHello(){
        try {
            Counter counter = counterService.getCounter();
            return "Hello, the counter is " + counter.getCount();

        }
        catch (Exception ex){
            log.trace(ex.getMessage());
        }
        return "Hello";
    }
}
