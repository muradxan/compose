package com.example.compose.service;

import com.example.compose.entity.Counter;

import java.util.List;

public interface CounterService {
    Counter getCounter();
}
