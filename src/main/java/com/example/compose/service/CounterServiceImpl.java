package com.example.compose.service;

import com.example.compose.entity.Counter;
import com.example.compose.repository.CounterRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CounterServiceImpl implements CounterService{
    private final CounterRepository repository;

    @Override
    public Counter getCounter() {
        Counter counter = repository.findById(1L).orElse(new Counter(0));
        counter.setCount(counter.getCount() + 1);
        return repository.save(counter);
    }
}
