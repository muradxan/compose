FROM alpine:3.14
RUN apk add --no-cache openjdk11
COPY build/libs/compose-0.0.1-SNAPSHOT.jar /app/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/compose-0.0.1-SNAPSHOT.jar"]
